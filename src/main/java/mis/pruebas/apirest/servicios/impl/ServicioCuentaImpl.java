package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    public final Map<String,Cuenta> cuentas = new ConcurrentHashMap<String,Cuenta>();

    @Override
    public List<Cuenta> obtenerCuentas() {
        return List.copyOf(this.cuentas.values());
    }

    @Override
    public void insertarCuentaNueva(Cuenta cuenta) {
        this.cuentas.put(cuenta.numero,cuenta) ;
    }

    @Override
    public Cuenta obtenerCuenta(String numero) {

        return this.cuentas.get(numero);
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        this.cuentas.replace(cuenta.numero, cuenta);
    }

    @Override
    public void emparcharCuenta(Cuenta parche) {

        final Cuenta existente = this.cuentas.get(parche.numero);

        if(parche.moneda != existente.moneda)
            existente.moneda = parche.moneda;

        if(parche.estado != null)
            existente.estado = parche.estado;

        if (parche.saldo != existente.saldo)
            existente.saldo = parche.saldo;

        if (parche.oficina != null)
            existente.oficina = parche.oficina;

        if (parche.tipo != null)
            existente.tipo = parche.tipo;

        this.cuentas.replace(existente.numero, existente);


    }

    @Override
    public void borrarCuenta(String numero) {
        this.cuentas.remove(numero);
    }
}
